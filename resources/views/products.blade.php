@extends("layout.app")

<body>
<div class="container">
    <x-TopNavbar></x-TopNavbar>
</div>
<x-breadcrumb></x-breadcrumb>
<div class="container-fluid">
<div class="d-block d-sm-none ">
    <x-topBanner></x-topBanner>
</div>
<div class="row d-flex justify-content-center">
    <div class="col-2 d-block d-sm-none d-lg-block">
        <x-leftSearch></x-leftSearch>
</div>
    <div class="row col-12 col-md-7 m-5  ">
<div class="d-none d-sm-block">
        <x-topBanner></x-topBanner>
</div>
        <div class="d-flex flex-row container-fluid">
        <x-ProductFilter></x-ProductFilter>
        </div>

    </div>
</div>
</div>
<div class="container-fluid m-0 p-0">
    <x-footer></x-footer>

</div>
</body>
</html>

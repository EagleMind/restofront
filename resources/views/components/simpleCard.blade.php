@extends("layout.app")
@section("content")
<div class="col-md-4">
    <a href="furniture.html#">
        <div class="collection-banner p-right text-right">
            <div class="img-part">
                <img src="../assets/images/furniture/2banner1.jpg" alt=""
                     class="img-fluid blur-up lazyload bg-img">
            </div>
            <div class="contain-banner banner-3">
                <div>
                    <h4>save 30%</h4>
                    <h2>sofa</h2>
                </div>
            </div>
        </div>
    </a>
</div>
@endsection

@extends("layout.app")
@section("content")
<div class="col-md-4">
    <a href="pets.html#">
        <div class="collection-banner p-left">
            <div class="img-part">
                <img src="../assets/images/pets/banner/1.jpg" class="img-fluid blur-up lazyload bg-img"
                     alt="">
            </div>
            <div class="contain-banner banner-3">
                <div>
                    <h2>Clothes</h2>
                </div>
            </div>
        </div>
    </a>
</div>
@endsection

@extends("layout.app")
@section("content")
<div class="col-lg-6 right-login">
    <h3>New Customer</h3>
    <div class="theme-card authentication-right">
        <h6 class="title-font">Create A Account</h6>
        <p>Sign up for a free account at our store. Registration is quick and easy. It allows you to be
            able to order from our shop. To start shopping click register.</p><a href="login.html#" class="btn btn-solid">Create an Account</a>
    </div>
</div>
@endsection

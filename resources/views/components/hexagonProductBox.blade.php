<section class="section-b-space">
    <div class="full-box">
        <div class="container">
            <div class="title4">
                <h2 class="title-inner4">best selling products</h2>
                <div class="line"><span></span></div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="theme-card center-align">
                        <div class="offer-slider">
                            <div class="sec-1">
                                <div class="product-box2">
                                    <div class="media">
                                        <a href="product-page(no-sidebar).html"><img
                                                class="img-fluid blur-up lazyload"
                                                src="{{asset("/assets/images/flower/product/1.jpg")}}" alt=""></a>
                                        <div class="media-body align-self-center">
                                            <div class="rating"><i class="fa fa-star"></i> <i
                                                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                    class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                            <a href="product-page(no-sidebar).html">
                                                <h6>Slim Fit Cotton Shirt</h6>
                                            </a>
                                            <h4>$500.00</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-box2">
                                    <div class="media">
                                        <a href="product-page(no-sidebar).html"><img
                                                class="img-fluid blur-up lazyload"
                                                src="{{asset("/assets/images/flower/product/2.jpg")}}" alt=""></a>
                                        <div class="media-body align-self-center">
                                            <div class="rating"><i class="fa fa-star"></i> <i
                                                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                    class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                            <a href="product-page(no-sidebar).html">
                                                <h6>Slim Fit Cotton Shirt</h6>
                                            </a>
                                            <h4>$500.00 <del>$600.00</del></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 center-slider">
                    <div>
                        <div class="offer-slider">
                            <div>
                                <div class="product-box product-wrap">
                                    <div class="img-wrapper">
                                        <div class="front text-center">
                                            <a href="flower.html#"><img src="{{asset("/assets/images/flower/product/7.jpg")}}"
                                                                        class="img-fluid blur-up lazyload" alt=""></a>
                                        </div>
                                        <div class="cart-box">
                                            <button data-toggle="modal" data-target="#addtocart"
                                                    title="Add to cart"><i class="ti-shopping-cart"></i></button> <a
                                                href="javascript:void(0)" title="Add to Wishlist"><i
                                                    class="ti-heart" aria-hidden="true"></i></a> <a href="flower.html#"
                                                                                                    data-toggle="modal" data-target="#quick-view" title="Quick View"><i
                                                    class="ti-search" aria-hidden="true"></i></a> <a
                                                href="compare.html" title="Compare"><i class="ti-reload"
                                                                                       aria-hidden="true"></i></a></div>
                                    </div>
                                    <div class="product-detail text-center">
                                        <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                class="fa fa-star"></i></div>
                                        <a href="product-page(no-sidebar).html">
                                            <h6>Slim Fit Cotton Shirt</h6>
                                        </a>
                                        <h4>$500.00</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="theme-card center-align">
                        <div class="offer-slider">
                            <div class="sec-1">
                                <div class="product-box2">
                                    <div class="media">
                                        <a href="product-page(no-sidebar).html"><img
                                                class="img-fluid blur-up lazyload"
                                                src="{{asset("/assets/images/flower/product/5.jpg")}}"alt=""></a>
                                        <div class="media-body align-self-center">
                                            <div class="rating"><i class="fa fa-star"></i> <i
                                                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                    class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                            <a href="product-page(no-sidebar).html">
                                                <h6>Slim Fit Cotton Shirt</h6>
                                            </a>
                                            <h4>$500.00</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-box2">
                                    <div class="media">
                                        <a href="product-page(no-sidebar).html"><img
                                                class="img-fluid blur-up lazyload"
                                                src="{{asset("/assets/images/flower/product/6.jpg")}}" alt=""></a>
                                        <div class="media-body align-self-center">
                                            <div class="rating"><i class="fa fa-star"></i> <i
                                                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                    class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                            <a href="product-page(no-sidebar).html">
                                                <h6>Slim Fit Cotton Shirt</h6>
                                            </a>
                                            <h4>$500.00</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

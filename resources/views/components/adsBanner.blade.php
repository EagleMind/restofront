<section class="section-b-space p-t-0 ratio_40">
    <div class="container">
        <div class="row partition2">
            <div class="col-md-6">
                <a href="shoes.html#">
                    <div class="collection-banner p-right text-center">
                        <div class="img-part">
                            <img src={{asset("/assets/images/sub-banner.png")}} class="img-fluid blur-up lazyload bg-img"
                                 alt="">
                        </div>
                        <div class="contain-banner">
                            <div>
                                <h4 class="text-white">50% off</h4>
                                <h2>men</h2>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="shoes.html#">
                    <div class="collection-banner p-right text-center">
                        <div class="img-part">
                            <img src={{asset("/assets/images/sub-banner1.png")}} class="img-fluid blur-up lazyload bg-img"
                                 alt="">
                        </div>
                        <div class="contain-banner">
                            <div>
                                <h4 class="text-white">20% save</h4>
                                <h2>women</h2>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

@extends("layout.app")
@section("content")
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="main-menu">
                <div class="menu-left">
                    <div class="navbar">
                        <a href="javascript:void(0)">
                            <div class="bar-style"><i class="fa fa-bars sidebar-bar" aria-hidden="true"></i>
                            </div>
                        </a>
                    </div>
                    <div class="brand-logo">
                        <a href="index.html"><img src="" class="img-fluid blur-up lazyload" alt="">Logo</a>
                    </div>
                </div>
                <div class="menu-right pull-right">
                    <div>
                        <nav>
                            <div class="toggle-nav"><i class="fa fa-bars sidebar-bar"></i></div>
                            <ul class="sm pixelstrap sm-horizontal">
                                <li>
                                    <div class="mobile-back text-right">Back<i
                                            class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
                                </li>
                                <li>
                                    <a href="index.html#">Home</a>
                                </li>
                                <li>
                                    <a href="index.html#">shop</a>
                                </li>
                                <li>
                                    <a href="index.html#">product</a>
                                </li>
                                <li class="mega"><a href="index.html#">features
                                    <div class="lable-nav">new</div>
                                </a>
                                </li>
                                <li><a href="index.html#">pages</a>
                                </li>
                                <li>
                                    <a href="index.html#">blog</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div>
                        <div class="icon-nav d-none d-sm-block">
                            <ul>
                                <li class="onhover-div mobile-search">
                                    <div><img src="../assets/images/icon/search.png" onclick="openSearch()"
                                              class="img-fluid blur-up lazyload" alt=""> <i class="ti-search"
                                                                                            onclick="openSearch()"></i></div>
                                </li>
                                <li class="onhover-div mobile-setting">
                                    <div><img src="../assets/images/icon/setting.png"
                                              class="img-fluid blur-up lazyload" alt=""> <i
                                            class="ti-settings"></i></div>
                                </li>
                                <li class="onhover-div mobile-cart">
                                    <div><img src="../assets/images/icon/cart.png"
                                              class="img-fluid blur-up lazyload" alt=""> <i
                                            class="ti-shopping-cart"></i></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

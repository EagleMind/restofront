<div class="bg-block">
    <section class="p-0">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="title4">
                        <h2 class="title-inner4">trending products</h2>
                        <div class="line"><span></span></div>
                    </div>
                    <div class="theme-tab">
                        <ul class="tabs tab-title">
                            <li class="current"><a href="http://themes.pixelstrap.com/multikart/front-end/tab-7">winter</a></li>
                            <li class=""><a href="http://themes.pixelstrap.com/multikart/front-end/tab-8">greens</a></li>
                            <li class=""><a href="http://themes.pixelstrap.com/multikart/front-end/tab-9">various</a></li>
                        </ul>
                        <div class="tab-content-cls">
                            <div id="tab-7" class="tab-content active default">
                                <div class="row product-tab">
                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/7.jpg")}}" alt=""></a>
                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00 <del>$600.00</del></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/7.jpg")}}" alt=""></a>
                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00 <del>$600.00</del></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/7.jpg")}}" alt=""></a>
                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00 <del>$600.00</del></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/7.jpg")}}" alt=""></a>
                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00 <del>$600.00</del></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/7.jpg")}}" alt=""></a>
                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00 <del>$600.00</del></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/7.jpg")}}" alt=""></a>
                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00 <del>$600.00</del></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/7.jpg")}}" alt=""></a>
                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00 <del>$600.00</del></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/7.jpg")}}" alt=""></a>
                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00 <del>$600.00</del></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="tab-8" class="tab-content">
                                <div class="row product-tab">
                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/7.jpg")}}" alt=""></a>
                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00 <del>$600.00</del></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/7.jpg")}}" alt=""></a>
                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00 <del>$600.00</del></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="tab-9" class="tab-content">
                                <div class="row product-tab">
                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/5.jpg")}}" alt=""></a>

                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/2.jpg")}}" alt=""></a>

                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/7.jpg")}}" alt=""></a>

                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/1.jpg")}}" alt=""></a>

                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/2.jpg")}}" alt=""></a>

                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/3.jpg")}}" alt=""></a>

                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/4.jpg")}}" alt=""></a>

                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00 <del>$600.00</del></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-box">
                                        <div class="product-box2">
                                            <div class="media">
                                                <a href="product-page(no-sidebar).html"><img
                                                        class="img-fluid blur-up lazyload"
                                                        src="{{asset("/assets/images/flower/product/5.jpg")}}" alt=""></a>

                                                <div class="media-body align-self-center">
                                                    <div class="rating"><i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                            class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                                                    <a href="product-page(no-sidebar).html">
                                                        <h6>Slim Fit Cotton Shirt</h6>
                                                    </a>
                                                    <h4>$500.00</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

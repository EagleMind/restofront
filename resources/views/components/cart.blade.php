<section class="cart-section section-b-space ">
    <div class="container">

        <div class="table-responsive">
            <table  style="width:100%" class="table  table-bordered">
                <thead>
                <tr>
                    <th scope="col">image</th>
                    <th scope="col">product name</th>
                    <th scope="col">price</th>
                    <th scope="col">quantity</th>
                    <th scope="col">action</th>
                    <th scope="col">total</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <a href="cart.html#"><img src="{{asset("/assets/images/pro3/1.jpg")}}" alt=""></a>
                    </td>
                    <td>
                        cotton shirt

                        </td>
                    <td>
                       $63.00
                    </td>
                    <td>
                        <div class="input-group">
                            <input type="text" name="quantity" class="form-control input-number"
                                   value="1">
                        </div>
                    </td>

                    <td><a href="cart.html#" class="icon"><i class="ti-close"></i></a></td>
                    <td>
                        <h2 class="td-color">$4539.00</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="cart.html#"><img src="{{asset("/assets/images/pro3/1.jpg")}}" alt=""></a>
                    </td>
                    <td>
                        cotton shirt

                    </td>
                    <td>
                        $63.00
                    </td>
                    <td>
                        <div class="input-group">
                            <input type="text" name="quantity" class="form-control input-number"
                                   value="1">
                        </div>
                    </td>

                    <td><a href="cart.html#" class="icon"><i class="ti-close"></i></a></td>
                    <td>
                        <h2 class="td-color">$4539.00</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="cart.html#"><img src="{{asset("/assets/images/pro3/1.jpg")}}" alt=""></a>
                    </td>
                    <td>
                        cotton shirt

                    </td>
                    <td>
                        $63.00
                    </td>
                    <td>
                        <div class="input-group">
                            <input type="text" name="quantity" class="form-control input-number"
                                   value="1">
                        </div>
                    </td>

                    <td><a href="cart.html#" class="icon"><i class="ti-close"></i></a></td>
                    <td>
                        <h2 class="td-color">$4539.00</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="cart.html#"><img src="{{asset("/assets/images/pro3/1.jpg")}}" alt=""></a>
                    </td>
                    <td>
                        cotton shirt

                    </td>
                    <td>
                        $63.00
                    </td>
                    <td>
                        <div class="input-group">
                            <input type="text" name="quantity" class="form-control input-number"
                                   value="1">
                        </div>
                    </td>

                    <td><a href="cart.html#" class="icon"><i class="ti-close"></i></a></td>
                    <td>
                        <h2 class="td-color">$4539.00</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="cart.html#"><img src="{{asset("/assets/images/pro3/1.jpg")}}" alt=""></a>
                    </td>
                    <td>
                        cotton shirt

                    </td>
                    <td>
                        $63.00
                    </td>
                    <td>
                        <div class="input-group">
                            <input type="text" name="quantity" class="form-control input-number"
                                   value="1">
                        </div>
                    </td>

                    <td><a href="cart.html#" class="icon"><i class="ti-close"></i></a></td>
                    <td>
                        <h2 class="td-color">$4539.00</h2>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>


                <table class="table cart-table table-responsive">
                    <tfoot>
                    <tr>
                        <td>total price :</td>
                        <td>
                            <h2>$6935.00</h2>
                        </td>
                    </tr>
                    </tfoot>
                </table>

        <div class="row cart-buttons">
            <div class="col-6"><a href="cart.html#" class="btn btn-solid">continue shopping</a></div>
            <div class="col-6"><a href="cart.html#" class="btn btn-solid">check out</a></div>
        </div>
    </div>
</section>

@extends("layout.app")
@section("content")
<div class="col-lg-4">
    <div class="product-right product-form-box">
        <h4><del>$459.00</del><span>55% off</span></h4>
        <h3>$32.96</h3>
        <ul class="color-variant">
            <li class="bg-light0"></li>
            <li class="bg-light1"></li>
            <li class="bg-light2"></li>
        </ul>
        <div class="product-description border-product">
            <h6 class="product-title">Time Reminder</h6>
            <div class="timer">
                <p id="demo"><span>25 <span class="padding-l">:</span> <span
                        class="timer-cal">Days</span> </span><span>22 <span
                        class="padding-l">:</span> <span class="timer-cal">Hrs</span>
                                        </span><span>13 <span class="padding-l">:</span> <span
                        class="timer-cal">Min</span> </span><span>57 <span
                        class="timer-cal">Sec</span></span>
                </p>
            </div>
            <h6 class="product-title">select size</h6>
            <div class="modal fade" id="sizemodal" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Sheer Straight Kurta</h5>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body"><img src="../assets/images/size-chart.jpg" alt=""
                                                     class="img-fluid blur-up lazyload"></div>
                    </div>
                </div>
            </div>
            <div class="size-box">
                <ul>
                    <li class="active"><a href="product-page(3-col-left).html#">s</a></li>
                    <li><a href="product-page(3-col-left).html#">m</a></li>
                    <li><a href="product-page(3-col-left).html#">l</a></li>
                    <li><a href="product-page(3-col-left).html#">xl</a></li>
                </ul>
            </div>
            <h6 class="product-title">quantity</h6>
            <div class="qty-box">
                <div class="input-group"><span class="input-group-prepend"><button type="button"
                                                                                   class="btn quantity-left-minus" data-type="minus" data-field=""><i
                        class="ti-angle-left"></i></button> </span>
                    <input type="text" name="quantity" class="form-control input-number" value="1">
                    <span class="input-group-prepend"><button type="button"
                                                              class="btn quantity-right-plus" data-type="plus" data-field=""><i
                            class="ti-angle-right"></i></button></span></div>
            </div>
        </div>
        <div class="product-buttons"><a href="product-page(3-col-left).html#" data-toggle="modal" data-target="#addtocart"
                                        class="btn btn-solid">add to cart</a> <a href="product-page(3-col-left).html#" class="btn btn-solid">buy now</a>
        </div>
    </div>
</div>
@endsection

@section("content")
<div class="product-right product-description-box">
    <h2>Women Pink Shirt</h2>
    <div class="border-product">
        <h6 class="product-title">product details</h6>
        <p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium
            doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore
            veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam
            voluptatem,</p>
    </div>
    <div class="single-product-tables border-product detail-section">
        <table>
            <tbody>
            <tr>
                <td>Febric:</td>
                <td>Chiffon</td>
            </tr>
            <tr>
                <td>Color:</td>
                <td>Red</td>
            </tr>
            <tr>
                <td>Material:</td>
                <td>Crepe printed</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="border-product">
        <h6 class="product-title">share it</h6>
        <div class="product-icon">
            <ul class="product-social">
                <li><a href="product-page(3-col-left).html#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="product-page(3-col-left).html#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="product-page(3-col-left).html#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="product-page(3-col-left).html#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="product-page(3-col-left).html#"><i class="fa fa-rss"></i></a></li>
            </ul>
            <form class="d-inline-block">
                <button class="wishlist-btn"><i class="fa fa-heart"></i><span
                        class="title-font">Add To WishList</span></button>
            </form>
        </div>
    </div>
    <div class="border-product">
        <h6 class="product-title">100% SECURE PAYMENT</h6>
        <div class="payment-card-bottom">
            <ul>
                <li>
                    <a href="product-page(3-col-left).html#"><img src="{{asset("/assets/images/icon/visa.png")}}" alt=""></a>
                </li>
                <li>
                    <a href="product-page(3-col-left).html#"><img src="{{asset("/assets/images/icon/mastercard.png")}}" alt=""></a>
                </li>
                <li>
                    <a href="product-page(3-col-left).html#"><img src="{{asset("/assets/images/icon/paypal.png")}}" alt=""></a>
                </li>
                <li>
                    <a href="product-page(3-col-left).html#"><img src="{{asset("/assets/images/icon/american-express.png")}}"
                                                                  alt=""></a>
                </li>
                <li>
                    <a href="product-page(3-col-left).html#"><img src="{{asset("/assets/images/icon/discover.png")}}" alt=""></a>
                </li>
            </ul>
        </div>
    </div>
</div>

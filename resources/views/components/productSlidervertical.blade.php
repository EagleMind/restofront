@extends("layout.app")
@section("content")
<div class="col-lg-4">
    <div class="theme-card card-border">
        <h5 class="title-border">new product</h5>
        <div class="offer-slider slide-1">
            <div>
                <div class="media">
                    <a href="product-page(no-sidebar).html"><img alt=""
                                                                 class="img-fluid blur-up lazyload" src="http://themes.pixelstrap.com/multikart/assets/images/bags/1.jpg"></a>
                    <div class="media-body align-self-center">
                        <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i></div>
                        <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                        </a>
                        <h4>$500.00</h4>
                    </div>
                </div>
                <div class="media">
                    <a href="product-page(no-sidebar).html"><img alt=""
                                                                 class="img-fluid blur-up lazyload" src="../assets/images/bags/6.jpg"></a>
                    <div class="media-body align-self-center">
                        <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i></div>
                        <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                        </a>
                        <h4>$500.00</h4>
                    </div>
                </div>
                <div class="media">
                    <a href="product-page(no-sidebar).html"><img alt=""
                                                                 class="img-fluid blur-up lazyload" src="../assets/images/bags/7.jpg"></a>
                    <div class="media-body align-self-center">
                        <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i></div>
                        <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                        </a>
                        <h4>$500.00</h4>
                    </div>
                </div>
                <div class="media">
                    <a href="product-page(no-sidebar).html"><img alt=""
                                                                 class="img-fluid blur-up lazyload" src="http://themes.pixelstrap.com/multikart/assets/images/bags/1.jpg"></a>
                    <div class="media-body align-self-center">
                        <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i></div>
                        <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                        </a>
                        <h4>$500.00</h4>
                    </div>
                </div>
            </div>
            <div>
                <div class="media">
                    <a href="product-page(no-sidebar).html"><img alt=""
                                                                 class="img-fluid blur-up lazyload" src="http://themes.pixelstrap.com/multikart/assets/images/bags/1.jpg"></a>
                    <div class="media-body align-self-center">
                        <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i></div>
                        <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                        </a>
                        <h4>$500.00</h4>
                    </div>
                </div>
                <div class="media">
                    <a href="product-page(no-sidebar).html"><img alt=""
                                                                 class="img-fluid blur-up lazyload" src="../assets/images/bags/6.jpg"></a>
                    <div class="media-body align-self-center">
                        <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i></div>
                        <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                        </a>
                        <h4>$500.00</h4>
                    </div>
                </div>
                <div class="media">
                    <a href="product-page(no-sidebar).html"><img alt=""
                                                                 class="img-fluid blur-up lazyload" src="../assets/images/bags/7.jpg"></a>
                    <div class="media-body align-self-center">
                        <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i></div>
                        <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                        </a>
                        <h4>$500.00 <del>$600.00</del></h4>
                    </div>
                </div>
                <div class="media">
                    <a href="product-page(no-sidebar).html"><img alt=""
                                                                 class="img-fluid blur-up lazyload" src="http://themes.pixelstrap.com/multikart/assets/images/bags/1.jpg"></a>
                    <div class="media-body align-self-center">
                        <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i></div>
                        <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                        </a>
                        <h4>$500.00</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

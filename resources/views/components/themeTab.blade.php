
<section class="section-b-space p-t-0 ratio_asos">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="theme-tab">
                    <ul class="tabs tab-title">
                        <li class="current"><a href="http://themes.pixelstrap.com/multikart/front-end/tab-4">New Products</a></li>
                        <li class=""><a href="http://themes.pixelstrap.com/multikart/front-end/tab-5">Featured Products</a></li>
                        <li class=""><a href="http://themes.pixelstrap.com/multikart/front-end/tab-6">Best Sellers</a></li>
                    </ul>
                    <div class="tab-content-cls">
                        <div id="tab-4" class="tab-content active default">
                            <div class="no-slider row">
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>

                            </div>
                        </div>
                        <div id="tab-5" class="tab-content">
                            <div class="no-slider row">
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>

                            </div>
                        </div>
                        <div id="tab-6" class="tab-content">
                            <div class="no-slider row">
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>
                                <x-card></x-card>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


 <header class="header-2">
     <div class="container">
         <div class="row">
             <div class="col-sm-12">
                 <div class="main-menu border-section border-top-0">
                     <div class="menu-left">
                         <div class="navbar">
                             <a href="javascript:void(0)" onclick="openNav()">

                             </a>
                         </div>
                     </div>
                     <div class="brand-logo layout2-logo">
                         <a href="furniture.html#"><img src="{{asset("assets/images/icon/logo/1.png")}}"
                                                        class="img-fluid blur-up lazyload" alt=""></a>
                     </div>
                     <div class="menu-right pull-right">
                         <div class="icon-nav d-none d-sm-block">
                             <ul>
                                 <li class="onhover-div mobile-search">
                                     <div><img src="{{asset("assets/images/icon/search.png")}}" onclick="openSearch()"
                                               class="img-fluid blur-up lazyload" alt=""> <i class="ti-search"
                                                                                             onclick="openSearch()"></i></div>
                                 </li>
                                 <li class="onhover-div mobile-setting">
                                     <div><img src="{{asset("assets/images/icon/setting.png")}}"
                                               class="img-fluid blur-up lazyload" alt=""> <i
                                             class="ti-settings"></i>
                                     </div>
                                 </li>
                                 <li class="onhover-div mobile-cart">
                                     <div><img src="{{asset("assets/images/icon/cart.png")}}"
                                               class="img-fluid blur-up lazyload" alt=""> <i
                                             class="ti-shopping-cart"></i></div>
                                 </li>
                             </ul>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>

     <div class="container">
         <div class="row">
             <div class="col-lg-12">
                 <div class="main-nav-center">
                     <nav id="main-nav">
                         <div class="toggle-nav"><i class="fa fa-bars sidebar-bar"></i></div>
                         <!-- Sample menu definition -->
                         <ul id="main-menu" class="sm pixelstrap sm-horizontal">
                             <li>
                                 <div class="mobile-back text-right">Back<i class="fa fa-angle-right pl-2"
                                                                            aria-hidden="true"></i></div>
                             </li>
                             <li>
                                 <a href="furniture.html#">Home</a>
                             </li>
                             <li>
                                 <a href="furniture.html#">shop</a>
                             </li>
                             <li>
                                 <a href="furniture.html#">product</a>
                             </li>
                             <li class="mega"><a href="furniture.html#">features
                                     <div class="lable-nav">new</div>
                                 </a>
                             </li>
                             <li><a href="furniture.html#">pages</a>
                             </li>
                             <li>
                                 <a href="furniture.html#">blog</a>
                             </li>
                         </ul>
                     </nav>
                 </div>
             </div>
         </div>
     </div>
 </header>

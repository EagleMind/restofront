$32.96@extends("layout.app")
@section("content")
<div class="product-box2">
    <div class="media">
        <a href="product-page(no-sidebar).html"><img
                class="img-fluid blur-up lazyload"
                src="../assets/images/nursery/pro/1.jpg" alt=""></a>
        <div class="media-body align-self-center">
            <div class="rating"><i class="fa fa-star"></i> <i
                    class="fa fa-star"></i> <i class="fa fa-star"></i>
                <i class="fa fa-star"></i> <i class="fa fa-star"></i>
            </div><a href="product-page(no-sidebar).html">
            <h6>Slim Fit Cotton Shirt</h6>
        </a>
            <h4>$500.00</h4>
        </div>
    </div>
</div>
@endsection

@extends("layout.app")
@section("content")
<div class="col-lg-3 col-sm-10 col-xs-12 order-up">
    <div class="product-right-slick">
        <div><img src="../assets/images/pro3/1.jpg" alt=""
                  class="img-fluid blur-up lazyload image_zoom_cls-0"></div>
        <div><img src="../assets/images/pro3/2.jpg" alt=""
                  class="img-fluid blur-up lazyload image_zoom_cls-1"></div>
        <div><img src="../assets/images/pro3/27.jpg" alt=""
                  class="img-fluid blur-up lazyload image_zoom_cls-2"></div>
        <div><img src="../assets/images/pro3/27.jpg" alt=""
                  class="img-fluid blur-up lazyload image_zoom_cls-3"></div>
    </div>
</div>
@endsection

<section class="p-0">
    <div class="slide-1 home-slider">
        <div>
            <div class="home text-left p-left">
                <img src="{{asset("assets/images/home-banner/33.jpg")}}" alt="" class="bg-img blur-up lazyload">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="slider-contain">
                                <div>
                                    <h4>save upto 10%</h4>
                                    <h1>plant collection</h1><a href="nursery.html#"
                                                                class="btn btn-outline btn-classic">shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="home text-left p-right">
                <img src="{{asset("assets/images/home-banner/32.jpg")}}" alt="" class="bg-img blur-up lazyload">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="slider-contain">
                                <div>
                                    <h4>save upto 20%</h4>
                                    <h1>new collection</h1><a href="nursery.html#" class="btn btn-outline btn-classic">shop
                                    now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

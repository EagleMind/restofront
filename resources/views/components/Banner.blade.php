<section class="banner-padding pb-0">
    <div class="container">
        <div class="row partition2">
            <div class="col-md-4">
                <a href="flower.html#">
                    <div class="collection-banner p-left text-center">
                        <div class="img-part">
                            <img src="{{asset("/assets/images/flower/sub-banner2.jpg")}}" class="img-fluid blur-up lazyload"
                                 alt="">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-8">
                <a href="flower.html#">
                    <div class="collection-banner p-right text-right">
                        <div class="img-part">
                            <img src="{{asset("/assets/images/flower/sub-banner1.jpg")}}" class="img-fluid blur-up lazyload"
                                 alt="">
                        </div>
                        <div class="contain-banner">
                            <div>
                                <h4>save 30%</h4>
                                <h2>hot deal</h2>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

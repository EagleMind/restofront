
<div class="row">

    <div class="col-lg-12">
        <h3 class="text-uppercase*">create account</h3>
        <div class="theme-card">
            @if (Session::has('errors'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <h4 class="alert-heading">Error!</h4>
                    <p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    </p>
                </div>
            @endif
            <form class="theme-form">
                <div class="form-row">
                    <div class="col-md-6">
                        <label for="email">First Name</label>
                        <input type="text" class="form-control" id="fname" placeholder="First Name"
                               required="">
                    </div>
                    <div class="col-md-6">
                        <label for="review">Last Name</label>
                        <input type="password" class="form-control" id="lname" placeholder="Last Name"
                               required="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6">
                        <label for="email">email</label>
                        <input type="text" class="form-control" id="email" placeholder="Email" required="">
                    </div>
                    <div class="col-md-6">
                        <label for="review">Password</label>
                        <input type="password" class="form-control " id="review"
                               placeholder="Enter your password" required="">
                    </div><a href="register.html#" class="btn btn-solid mt-4 m-2">create Account</a>
                </div>
            </form>
        </div>
    </div>
</div>

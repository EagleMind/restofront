@extends("layout.app")
@section("content")
<div class="col-md-12">
    <a href="index.html#">
        <div class="classic-effect">
            <div>
                <img src="../assets/images/blog/1.jpg" class="img-fluid blur-up lazyload bg-img"
                     alt="">
            </div>
            <span></span>
        </div>
    </a>
    <div class="blog-details">
        <h4>25 January 2018</h4>
        <a href="index.html#">
            <p>Lorem ipsum dolor sit consectetur adipiscing elit,</p>
        </a>
        <hr class="style1">
        <h6>by: John Dio , 2 Comment</h6>
    </div>
</div>
@endsection

@extends("layout.app")
@section("content")
<div class="col-lg-3 col-sm-6">
    <div class="theme-card">
        <h5 class="title-border">feature product</h5>
        <div class="offer-slider slide-1">
            <div>
                <div class="media">
                    <a href="product-page(no-sidebar).html"><img class="img-fluid blur-up lazyload"
                                                                 src="../assets/images/game/pro/1.jpg" alt=""></a>
                    <div class="media-body align-self-center">
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                        </a>
                        <h4>$500.00 <del>$600.00</del></h4>
                    </div>
                </div>
                <div class="media">
                    <a href="product-page(no-sidebar).html"><img class="img-fluid blur-up lazyload"
                                                                 src="../assets/images/game/pro/2.jpg" alt=""></a>
                    <div class="media-body align-self-center">
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                        </a>
                        <h4>$500.00</h4>
                    </div>
                </div>
            </div>
            <div>
                <div class="media">
                    <a href="product-page(no-sidebar).html"><img class="img-fluid blur-up lazyload"
                                                                 src="../assets/images/game/pro/3.jpg" alt=""></a>
                    <div class="media-body align-self-center">
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                        </a>
                        <h4>$500.00 <del>$600.00</del></h4>
                    </div>
                </div>
                <div class="media">
                    <a href="product-page(no-sidebar).html"><img class="img-fluid blur-up lazyload"
                                                                 src="../assets/images/game/pro/4.jpg" alt=""></a>
                    <div class="media-body align-self-center">
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                        </a>
                        <h4>$500.00</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

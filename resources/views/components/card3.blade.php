
<div class="product-box">
    <div class="img-wrapper">
        <div class="front">
            <a href="product-page(no-sidebar).html"><img src="{{asset("/assets/images/beauty/pro/1.jpg")}}"
                                                         class="img-fluid blur-up lazyload " alt=""></a>
        </div>
        <div class="back">
            <a href="product-page(no-sidebar).html"><img src="{{asset("/assets/images/beauty/pro/2.jpg")}}"
                                                         class="img-fluid blur-up lazyload" alt=""></a>
        </div>
        <div class="cart-info cart-wrap">
            <button data-toggle="modal" data-target="#addtocart" title="Add to cart"><i
                    class="ti-shopping-cart"></i></button>
            <a href="javascript:void(0)" title="Add to Wishlist"><i class="ti-heart"
                                                                    aria-hidden="true"></i></a>
            <a href="beauty.html#" data-toggle="modal" data-target="#quick-view" title="Quick View"><i
                    class="ti-search" aria-hidden="true"></i></a>
            <a href="compare.html" title="Compare"><i class="ti-reload"
                                                      aria-hidden="true"></i></a>
        </div>
    </div>
    <div class="product-detail mt-3">

        <a href="product-page(no-sidebar).html">
            <h6>Slim Fit Cotton Shirt</h6>
        </a>
        <h4>$500.00 <del>$600.00</del></h4>
    </div>
</div>


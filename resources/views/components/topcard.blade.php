@extends("layout.app")
<div class="product-4 product-m no-arrow">
    <div class="product-box">
        <div class="img-wrapper">
            <div class="front">
                <a href="product-page(no-sidebar).html"><img src="../assets/images/pets/pro/10.jpg"
                                                             class="img-fluid blur-up lazyload bg-img" alt=""></a>
            </div>
            <div class="cart-info cart-wrap">
                <button data-toggle="modal" data-target="#addtocart" title="Add to cart"><i
                        class="ti-shopping-cart"></i></button>
                <a href="javascript:void(0)" title="Add to Wishlist"><i class="ti-heart"
                                                                        aria-hidden="true"></i></a>
                <a href="pets.html#" data-toggle="modal" data-target="#quick-view" title="Quick View"><i
                        class="ti-search" aria-hidden="true"></i></a>
                <a href="compare.html" title="Compare"><i class="ti-reload"
                                                          aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="product-detail">
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
            <a href="product-page(no-sidebar).html">
                <h6>Slim Fit Cotton Shirt</h6>
            </a>
            <h4>$500.00</h4>
        </div>
    </div>
    <div class="product-box">
        <div class="img-wrapper">
            <div class="lable-block">
                <span class="lable3">new</span>
                <span class="lable4">on sale</span>
            </div>
            <!-- front image -->
            <div class="front">
                <a href="product-page(no-sidebar).html"><img src="../assets/images/pets/pro/13.jpg"
                                                             class="img-fluid blur-up lazyload bg-img" alt=""></a>
            </div>
            <div class="cart-info cart-wrap">
                <button data-toggle="modal" data-target="#addtocart" title="Add to cart"><i
                        class="ti-shopping-cart"></i></button>
                <a href="javascript:void(0)" title="Add to Wishlist"><i class="ti-heart"
                                                                        aria-hidden="true"></i></a>
                <a href="pets.html#" data-toggle="modal" data-target="#quick-view" title="Quick View"><i
                        class="ti-search" aria-hidden="true"></i></a>
                <a href="compare.html" title="Compare"><i class="ti-reload"
                                                          aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="product-detail">
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
            <a href="product-page(no-sidebar).html">
                <h6>Slim Fit Cotton Shirt</h6>
            </a>
            <h4>$500.00 <del>$600.00</del></h4>
        </div>
    </div>
    <div class="product-box">
        <div class="img-wrapper">
            <div class="lable-block">
                <span class="lable3">new</span>
                <span class="lable4">on sale</span>
            </div>
            <div class="front">
                <a href="product-page(no-sidebar).html"><img src="../assets/images/pets/pro/12.jpg"
                                                             class="img-fluid blur-up lazyload bg-img" alt=""></a>
            </div>
            <div class="cart-info cart-wrap">
                <button data-toggle="modal" data-target="#addtocart" title="Add to cart"><i
                        class="ti-shopping-cart"></i></button>
                <a href="javascript:void(0)" title="Add to Wishlist"><i class="ti-heart"
                                                                        aria-hidden="true"></i></a>
                <a href="pets.html#" data-toggle="modal" data-target="#quick-view" title="Quick View"><i
                        class="ti-search" aria-hidden="true"></i></a>
                <a href="compare.html" title="Compare"><i class="ti-reload"
                                                          aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="product-detail">
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
            <a href="product-page(no-sidebar).html">
                <h6>Slim Fit Cotton Shirt</h6>
            </a>
            <h4>$500.00</h4>
        </div>
    </div>
    <div class="product-box">
        <div class="img-wrapper">
            <div class="front">
                <a href="product-page(no-sidebar).html"><img src="../assets/images/pets/pro/11.jpg"
                                                             class="img-fluid blur-up lazyload bg-img" alt=""></a>
            </div>
            <div class="cart-info cart-wrap">
                <button data-toggle="modal" data-target="#addtocart" title="Add to cart"><i
                        class="ti-shopping-cart"></i></button>
                <a href="javascript:void(0)" title="Add to Wishlist"><i class="ti-heart"
                                                                        aria-hidden="true"></i></a>
                <a href="pets.html#" data-toggle="modal" data-target="#quick-view" title="Quick View"><i
                        class="ti-search" aria-hidden="true"></i></a>
                <a href="compare.html" title="Compare"><i class="ti-reload"
                                                          aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="product-detail">
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
            <a href="product-page(no-sidebar).html">
                <h6>Slim Fit Cotton Shirt</h6>
            </a>
            <h4>$500.00</h4>
        </div>
    </div>
    <div class="product-box">
        <div class="img-wrapper">
            <div class="lable-block">
                <span class="lable3">new</span>
                <span class="lable4">on sale</span>
            </div>
            <div class="front">
                <a href="product-page(no-sidebar).html"><img src="../assets/images/pets/pro/5.jpg"
                                                             class="img-fluid blur-up lazyload bg-img" alt=""></a>
            </div>
            <div class="cart-info cart-wrap">
                <button data-toggle="modal" data-target="#addtocart" title="Add to cart"><i
                        class="ti-shopping-cart"></i></button>
                <a href="javascript:void(0)" title="Add to Wishlist"><i class="ti-heart"
                                                                        aria-hidden="true"></i></a>
                <a href="pets.html#" data-toggle="modal" data-target="#quick-view" title="Quick View"><i
                        class="ti-search" aria-hidden="true"></i></a>
                <a href="compare.html" title="Compare"><i class="ti-reload"
                                                          aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="product-detail">
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
            <a href="product-page(no-sidebar).html">
                <h6>Slim Fit Cotton Shirt</h6>
            </a>
            <h4>$500.00</h4>
        </div>
    </div>
</div>

<section class="section-b-space ratio_square">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="title4">
                    <h2 class="title-inner4">trending items</h2>
                    <div class="line"><span></span></div>
                </div>
                <div class="product-4 product-m no-arrow">
                    <div class="product-box product-wrap">
                        <div class="img-wrapper">
                            <div class="lable-block"><span class="lable3">new</span> <span class="lable4">on
                                        sale</span></div>
                            <div class="front">
                                <a href="product-page(no-sidebar).html"><img
                                        src="{{asset("/assets/images/flower/product/7.jpg")}}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="back">
                                <a href="product-page(no-sidebar).html"><img
                                        src="{{asset("/assets/images/flower/product/6.jpg")}}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="cart-box" style="background-color: #00CC99">
                                <button class="btn btn-success text-dark"  style="color: white !important;"> Order Now</button>

                            </div>
                        </div>
                        <div class="product-detail text-center">
                            <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                            </div>
                            <a href="product-page(no-sidebar).html">
                                <h6>Slim Fit Cotton Shirt</h6>
                            </a>
                            <h4>$500.00</h4>
                        </div>
                    </div>

                    <div class="product-box product-wrap">
                        <div class="img-wrapper">
                            <div class="lable-block"><span class="lable3">new</span> <span class="lable4">on
                                        sale</span></div>
                            <div class="front">
                                <a href="product-page(no-sidebar).html"><img
                                        src="{{asset("/assets/images/flower/product/7.jpg")}}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="back">
                                <a href="product-page(no-sidebar).html"><img
                                        src="{{asset("/assets/images/flower/product/6.jpg")}}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="cart-box" style="background-color: #00CC99">
                                <button class="btn btn-success text-dark"  style="color: white !important;"> Order Now</button>

                            </div>
                        </div>
                        <div class="product-detail text-center">
                            <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                            </div>
                            <a href="product-page(no-sidebar).html">
                                <h6>Slim Fit Cotton Shirt</h6>
                            </a>
                            <h4>$500.00</h4>
                        </div>
                    </div>

                    <div class="product-box product-wrap">
                        <div class="img-wrapper">
                            <div class="lable-block"><span class="lable3">new</span> <span class="lable4">on
                                        sale</span></div>
                            <div class="front">
                                <a href="product-page(no-sidebar).html"><img
                                        src="{{asset("/assets/images/flower/product/7.jpg")}}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="back">
                                <a href="product-page(no-sidebar).html"><img
                                        src="{{asset("/assets/images/flower/product/6.jpg")}}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="cart-box" style="background-color: #00CC99">
                                <button class="btn btn-success text-dark"  style="color: white !important;"> Order Now</button>

                            </div>
                        </div>
                        <div class="product-detail text-center">
                            <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                            </div>
                            <a href="product-page(no-sidebar).html">
                                <h6>Slim Fit Cotton Shirt</h6>
                            </a>
                            <h4>$500.00</h4>
                        </div>
                    </div>

                    <div class="product-box product-wrap">
                        <div class="img-wrapper">
                            <div class="lable-block"><span class="lable3">new</span> <span class="lable4">on
                                        sale</span></div>
                            <div class="front">
                                <a href="product-page(no-sidebar).html"><img
                                        src="{{asset("/assets/images/flower/product/7.jpg")}}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="back">
                                <a href="product-page(no-sidebar).html"><img
                                        src="{{asset("/assets/images/flower/product/6.jpg")}}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="cart-box" style="background-color: #00CC99">
                                <button class="btn btn-success text-dark"  style="color: white !important;"> Order Now</button>

                            </div>
                        </div>
                        <div class="product-detail text-center">
                            <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                            </div>
                            <a href="product-page(no-sidebar).html">
                                <h6>Slim Fit Cotton Shirt</h6>
                            </a>
                            <h4>$500.00</h4>
                        </div>
                    </div>

                    <div class="product-box product-wrap">
                        <div class="img-wrapper">
                            <div class="lable-block"><span class="lable3">new</span> <span class="lable4">on
                                        sale</span></div>
                            <div class="front">
                                <a href="product-page(no-sidebar).html"><img
                                        src="{{asset("/assets/images/flower/product/7.jpg")}}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="back">
                                <a href="product-page(no-sidebar).html"><img
                                        src="{{asset("/assets/images/flower/product/6.jpg")}}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="cart-box" style="background-color: #00CC99">
                                <button class="btn btn-success text-dark"  style="color: white !important;"> Order Now</button>

                            </div>
                        </div>
                        <div class="product-detail text-center">
                            <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                            </div>
                            <a href="product-page(no-sidebar).html">
                                <h6>Slim Fit Cotton Shirt</h6>
                            </a>
                            <h4>$500.00</h4>
                        </div>
                    </div>

                    <div class="product-box product-wrap">
                        <div class="img-wrapper">
                            <div class="lable-block"><span class="lable3">new</span> <span class="lable4">on
                                        sale</span></div>
                            <div class="front">
                                <a href="product-page(no-sidebar).html"><img
                                        src="{{asset("/assets/images/flower/product/7.jpg")}}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="back">
                                <a href="product-page(no-sidebar).html"><img
                                        src="{{asset("/assets/images/flower/product/6.jpg")}}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="cart-box" style="background-color: #00CC99">
                                <button class="btn btn-success text-dark"  style="color: white !important;"> Order Now</button>

                            </div>
                        </div>
                        <div class="product-detail text-center">
                            <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                            </div>
                            <a href="product-page(no-sidebar).html">
                                <h6>Slim Fit Cotton Shirt</h6>
                            </a>
                            <h4>$500.00</h4>
                        </div>
                    </div>

                    <div class="product-box product-wrap">
                        <div class="img-wrapper">
                            <div class="lable-block"><span class="lable3">new</span> <span class="lable4">on
                                        sale</span></div>
                            <div class="front">
                                <a href="product-page(no-sidebar).html"><img
                                        src="{{asset("/assets/images/flower/product/7.jpg")}}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="back">
                                <a href="product-page(no-sidebar).html"><img
                                        src="{{asset("/assets/images/flower/product/6.jpg")}}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="cart-box" style="background-color: #00CC99">
                                <button class="btn btn-success text-dark"  style="color: white !important;"> Order Now</button>

                            </div>
                        </div>
                        <div class="product-detail text-center">
                            <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                            </div>
                            <a href="product-page(no-sidebar).html">
                                <h6>Slim Fit Cotton Shirt</h6>
                            </a>
                            <h4>$500.00</h4>
                        </div>
                    </div>

        </div>
    </div>
</section>

<div class="container ratio_square">
    <section class="section-b-space border-section border-top-0">
        <div class="row">
            <div class="col">
<div class="product-4 product-m no-arrow">
    <div class="product-box product-wrap">
        <div class="img-wrapper">
            <div class="lable-block"><span class="lable3">new</span></div>
            <div class="front">
                <a href="product-page(no-sidebar).html"><img alt=""
                                                             src="{{"assets/images/bags/7.jpg"}}"
                                                             class="img-fluid blur-up lazyload bg-img"></a>
            </div>
            <div class="back">
                <a href="product-page(no-sidebar).html"><img alt=""
                                                             src="{{"assets/images/bags/9.jpg"}}"
                                                             class="img-fluid blur-up lazyload bg-img"></a>
            </div>
            <div class="cart-detail"><a href="javascript:void(0)" title="Add to Wishlist"><i
                        class="ti-heart" aria-hidden="true"></i></a> <a href="bags.html#" data-toggle="modal"
                                                                        data-target="#quick-view" title="Quick View"><i class="ti-search"
                                                                                                                        aria-hidden="true"></i></a> <a href="compare.html" title="Compare"><i
                        class="ti-reload" aria-hidden="true"></i></a></div>
        </div>
        <div class="product-info">
            <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
            </div>
            <a href="product-page(no-sidebar).html">
                <h6>Slim Fit Cotton Shirt</h6>
            </a>
            <h4>$500.00</h4>
            <ul class="color-variant">
                <li class="bg-light0"></li>
                <li class="bg-light1"></li>
                <li class="bg-light2"></li>
            </ul>
            <div class="add-btn">
                <a href="javascript:void(0)" onclick="openCart()" class="btn btn-outline">
                    <i class="ti-shopping-cart"></i> add to cart
                </a>
            </div>
        </div>
    </div>
    <div class="product-box product-wrap">
        <div class="img-wrapper">
            <div class="front">
                <a href="product-page(no-sidebar).html"><img alt=""
                                                             src="{{"assets/images/bags/19.jpg"}}"
                                                             class="img-fluid blur-up lazyload bg-img"></a>
            </div>
            <div class="back">
                <a href="product-page(no-sidebar).html"><img alt=""
                                                             src="{{"assets/images/bags/20.jpg"}}"
                                                             class="img-fluid blur-up lazyload bg-img"></a>
            </div>
            <div class="cart-detail"><a href="javascript:void(0)" title="Add to Wishlist"><i
                        class="ti-heart" aria-hidden="true"></i></a> <a href="bags.html#" data-toggle="modal"
                                                                        data-target="#quick-view" title="Quick View"><i class="ti-search"
                                                                                                                        aria-hidden="true"></i></a> <a href="compare.html" title="Compare"><i
                        class="ti-reload" aria-hidden="true"></i></a></div>
        </div>
        <div class="product-info">
            <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
            </div>
            <a href="product-page(no-sidebar).html">
                <h6>Slim Fit Cotton Shirt</h6>
            </a>
            <h4>$500.00 <del>$600.00</del></h4>
            <ul class="color-variant">
                <li class="bg-light0"></li>
                <li class="bg-light1"></li>
                <li class="bg-light2"></li>
            </ul>
            <div class="add-btn">
                <a href="javascript:void(0)" onclick="openCart()" class="btn btn-outline">
                    <i class="ti-shopping-cart"></i> add to cart
                </a>
            </div>
        </div>
    </div>
    <div class="product-box product-wrap">
        <div class="img-wrapper">
            <div class="lable-block"><span class="lable3">new</span></div>
            <div class="front">
                <a href="product-page(no-sidebar).html"><img alt=""
                                                             src="{{"assets/images/bags/23.jpg"}}"
                                                             class="img-fluid blur-up lazyload bg-img"></a>
            </div>
            <div class="back">
                <a href="product-page(no-sidebar).html"><img alt=""
                                                             src="{{"assets/images/bags/24.jpg"}}"
                                                             class="img-fluid blur-up lazyload bg-img"></a>
            </div>
            <div class="cart-detail"><a href="javascript:void(0)" title="Add to Wishlist"><i
                        class="ti-heart" aria-hidden="true"></i></a> <a href="bags.html#" data-toggle="modal"
                                                                        data-target="#quick-view" title="Quick View"><i class="ti-search"
                                                                                                                        aria-hidden="true"></i></a> <a href="compare.html" title="Compare"><i
                        class="ti-reload" aria-hidden="true"></i></a></div>
        </div>
        <div class="product-info">
            <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
            </div>
            <a href="product-page(no-sidebar).html">
                <h6>Slim Fit Cotton Shirt</h6>
            </a>
            <h4>$500.00</h4>
            <ul class="color-variant">
                <li class="bg-light0"></li>
                <li class="bg-light1"></li>
                <li class="bg-light2"></li>
            </ul>
            <div class="add-btn">
                <a href="javascript:void(0)" onclick="openCart()" class="btn btn-outline">
                    <i class="ti-shopping-cart"></i> add to cart
                </a>
            </div>
        </div>
    </div>
    <div class="product-box product-wrap">
        <div class="img-wrapper">
            <div class="front">
                <a href="product-page(no-sidebar).html"><img alt=""
                                                             src="http://themes.pixelstrap.com/multikart/assets/images/bags/1.jpg"
                                                             class="img-fluid blur-up lazyload bg-img"></a>
            </div>
            <div class="back">
                <a href="product-page(no-sidebar).html"><img alt=""
                                                             src="{{"assets/images/bags/6.jpg"}}"
                                                             class="img-fluid blur-up lazyload bg-img"></a>
            </div>
            <div class="cart-detail"><a href="javascript:void(0)" title="Add to Wishlist"><i
                        class="ti-heart" aria-hidden="true"></i></a> <a href="bags.html#" data-toggle="modal"
                                                                        data-target="#quick-view" title="Quick View"><i class="ti-search"
                                                                                                                        aria-hidden="true"></i></a> <a href="compare.html" title="Compare"><i
                        class="ti-reload" aria-hidden="true"></i></a></div>
        </div>
        <div class="product-info">
            <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
            </div>
            <a href="product-page(no-sidebar).html">
                <h6>Slim Fit Cotton Shirt</h6>
            </a>
            <h4>$500.00</h4>
            <ul class="color-variant">
                <li class="bg-light0"></li>
                <li class="bg-light1"></li>
                <li class="bg-light2"></li>
            </ul>
            <div class="add-btn">
                <a href="javascript:void(0)" onclick="openCart()" class="btn btn-outline">
                    <i class="ti-shopping-cart"></i> add to cart
                </a>
            </div>
        </div>
    </div>
    <div class="product-box product-wrap">
        <div class="img-wrapper">
            <div class="lable-block"><span class="lable3">new</span></div>
            <div class="front">
                <a href="product-page(no-sidebar).html"><img alt=""
                                                             src="{{"assets/images/bags/23.jpg"}}"
                                                             class="img-fluid blur-up lazyload bg-img"></a>
            </div>
            <div class="back">
                <a href="product-page(no-sidebar).html"><img alt=""
                                                             src="{{"assets/images/bags/24.jpg"}}"
                                                             class="img-fluid blur-up lazyload bg-img"></a>
            </div>
            <div class="cart-detail"><a href="javascript:void(0)" title="Add to Wishlist"><i
                        class="ti-heart" aria-hidden="true"></i></a> <a href="bags.html#" data-toggle="modal"
                                                                        data-target="#quick-view" title="Quick View"><i class="ti-search"
                                                                                                                        aria-hidden="true"></i></a> <a href="compare.html" title="Compare"><i
                        class="ti-reload" aria-hidden="true"></i></a></div>
        </div>
        <div class="product-info">
            <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                    class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
            </div>
            <a href="product-page(no-sidebar).html">
                <h6>Slim Fit Cotton Shirt</h6>
            </a>
            <h4>$500.00</h4>
            <ul class="color-variant">
                <li class="bg-light0"></li>
                <li class="bg-light1"></li>
                <li class="bg-light2"></li>
            </ul>
            <div class="add-btn">
                <a href="javascript:void(0)" onclick="openCart()" class="btn btn-outline">
                    <i class="ti-shopping-cart"></i> add to cart
                </a>
            </div>
        </div>
    </div>
</div>
            </div>
        </div>
    </section>
</div>

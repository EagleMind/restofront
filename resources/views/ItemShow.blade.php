@extends("layout.app")
<body>
<div class="container">
    <x-TopNavbar></x-TopNavbar>
</div>
<x-breadcrumb></x-breadcrumb>
<div class="container-fluid">

    <div class="row d-flex justify-content-between">

        <div class="col-12 col-md-12 col-lg-12">
            <x-productShow4imgs></x-productShow4imgs>
            <x-relatedProducts></x-relatedProducts>

        </div>
    </div>
</div>
<div class="container-fluid m-0 p-0">
    <x-footer></x-footer>

</div>
</body>

@extends("layout.app")
    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->

</head>
<body>
<div class="container">
    <x-TopNavbar></x-TopNavbar>
</div>
<div class="container-fluid section-b-space">
   <x-thankYou></x-thankYou>

</div>
<div class="container-fluid m-0 p-0">
    <x-footer></x-footer>
</div>
</body>
</html>

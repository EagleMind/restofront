@extends("layout.app")
    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->

</head>
<body>
<div class="container">
    <x-TopNavbar></x-TopNavbar>
</div>
    <x-HomeSlider></x-HomeSlider>
<div class="container">
    <x-Banner></x-Banner>
</div>
<x-hexagonProductBox></x-hexagonProductBox>
<x-tabs></x-tabs>
<x-cardWithHover></x-cardWithHover>
<x-homeSlider></x-homeSlider>
<x-footer></x-footer>
</body>
</html>

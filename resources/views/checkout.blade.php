@extends("layout.app")
<body>
<div class="container">
    <x-TopNavbar></x-TopNavbar>
</div>
<x-breadcrumb></x-breadcrumb>
<x-orderDetails></x-orderDetails>
<div class="container-fluid m-0 p-0">
    <x-footer></x-footer>

</div>
</body>

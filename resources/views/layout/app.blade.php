<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="multikart">
    <meta name="keywords" content="multikart">
    <meta name="author" content="multikart">
    <title>Multikart - Multi-purpopse E-commerce Html Template</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

    <!-- Icons -->
    <link rel="stylesheet" type="text/css" href={{asset("assets/css/fontawesome.css")}}>

    <!--Slick slider css-->
    <link rel="stylesheet" type="text/css" href={{asset("assets/css/slick.css")}}>
    <link rel="stylesheet" type="text/css" href={{asset("assets/css/slick-theme.css")}}>

    <!-- Animate icon -->
    <link rel="stylesheet" type="text/css" href={{asset("assets/css/animate.css")}}>

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href={{asset("assets/css/themify-icons.css")}}>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <!-- Bootstrap css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- Theme css -->
    <link rel="stylesheet" type="text/css" href={{asset("assets/css/color3.css")}}>
    <link rel="stylesheet" type="text/css" href={{asset("assets/css/color1.css")}}>


</head>

<body>

<!-- latest jquery-->
<script src="{{asset("/assets/js/jquery-3.3.1.min.js")}}"></script>

<!-- popper js-->
<script src="{{asset("/assets/js/jquery-3.3.1.min.js")}}"></script>

<!-- slick js-->
<script src="{{asset("/assets/js/slick.js")}}"></script>

<script src="{{asset("/assets/js/menu.js")}}"></script>

<!-- lazyload js-->
<script src="{{asset("/assets/js/lazysizes.min.js")}}"></script>

<!-- Bootstrap js-->
<script src="{{asset("/assets/js/bootstrap.js")}}"></script>

<!-- Bootstrap Notification js-->
<script src="{{asset("/assets/js/bootstrap-notify.min.js")}}"></script>
<script src="{{asset("/assets/js/script.js")}}"></script>


<!-- Theme js-->


<script>
    $(window).on('load', function () {
        setTimeout(function () {
            $('#exampleModal').modal('show');
        }, 2500);
    });
    function openSearch() {
        document.getElementById("search-overlay").style.display = "block";
    }

    function closeSearch() {
        document.getElementById("search-overlay").style.display = "none";
    }
</script>
</body>
</html>

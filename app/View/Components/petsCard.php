<?php
namespace App\View\Components;
use Illuminate\View\Component;
class PetsCard extends Component
{
 /**
     * @return void
 */
    public function __construct()
    {
    }
 /**
     * @return \Illuminate\View\View|string
 */
    public function render()
    {
        return view('components.petsCard');
}
}

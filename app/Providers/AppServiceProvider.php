<?php

namespace App\Providers;

use App\View\Components\Banner;
use App\View\Components\BreadCrumb;
use App\View\Components\cardWithButton;
use App\View\Components\cardWithHover;
use App\View\Components\Footer;
use App\View\Components\hexagonProductBox;
use App\View\Components\leftMenu;
use App\View\Components\LeftSearch;
use App\View\Components\productFilter;
use App\View\Components\productShow;
use App\View\Components\productShow4imgs;
use App\View\Components\relatedProducts;
use App\View\Components\signIn;
use App\View\Components\tabs;
use App\View\Components\thankYou;
use App\View\Components\themeTab;
use App\View\Components\TopBanner;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use App\View\Components\AccounceBanner;
use App\View\Components\AddToCart;
use App\View\Components\AdsBanner;
use App\View\Components\Alertt;
use App\View\Components\Card;
use App\View\Components\Card2;
use App\View\Components\Card3;
use App\View\Components\CardInSlide;
use App\View\Components\Cart;
use App\View\Components\CheckoutInfos;
use App\View\Components\CollectionCard;
use App\View\Components\CreateAccount;
use App\View\Components\DashboardInfoView;
use App\View\Components\HomeSlider;
use App\View\Components\Instareferer;
use App\View\Components\Login;
use App\View\Components\Navbar;
use App\View\Components\OrderDetails;
use App\View\Components\PersonalDetails;
use App\View\Components\PetsCard;
use App\View\Components\Productbox;
use App\View\Components\Productinfos;
use App\View\Components\ProductPicView;
use App\View\Components\ProductsCommandDetails;
use App\View\Components\ProductSlidervertical;
use App\View\Components\Service;
use App\View\Components\ShippingDetails;
use App\View\Components\SignUp;
use App\View\Components\SimpleCard;
use App\View\Components\SingleProductPicView;
use App\View\Components\Slider;
use App\View\Components\SmallCardSlide;
use App\View\Components\TabsNav;
use App\View\Components\Topcard;
use App\View\Components\TopNavbar;
use App\View\Components\sectionText;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('cardWithHover', cardWithHover::class);
        Blade::component('tabs', tabs::class);
        Blade::component('hexagonProductBox', hexagonProductBox::class);
        Blade::component('Banner', Banner::class);
        Blade::component('signIn', signIn::class);
        Blade::component('signUp', signUp::class);
        Blade::component('thankYou', thankYou::class);
        Blade::component('cardWithButton', cardWithButton::class);
        Blade::component('relatedProducts', relatedProducts::class);
        Blade::component('productShow4imgs', productShow4imgs::class);
        Blade::component('AccounceBanner', AccounceBanner::class);
        Blade::component('leftMenu', leftMenu::class);
        Blade::component('productShow', productShow::class);
        Blade::component('productFilter', productFilter::class);
        Blade::component('topbanner', TopBanner::class);
        Blade::component('LeftSearch', LeftSearch::class);
        Blade::component('BreadCrumb', BreadCrumb::class);
        Blade::component('Footer', Footer::class);
        Blade::component('themeTab', themeTab::class);
        Blade::component('AddToCart', AddToCart::class);
        Blade::component('AdsBanner', AdsBanner::class);
        Blade::component('Alertt', Alertt::class);
        Blade::component('Card', Card::class);
        Blade::component('Card2', Card2::class);
        Blade::component('Card3', Card3::class);
        Blade::component('CardInSlide', CardInSlide::class);
        Blade::component('Cart', Cart::class);
        Blade::component('CheckoutInfos', CheckoutInfos::class);
        Blade::component('CollectionCard', CollectionCard::class);
        Blade::component('CreateAccount', CreateAccount::class);
        Blade::component('DashboardInfoView', DashboardInfoView::class);
        Blade::component('HomeSlider', HomeSlider::class);
        Blade::component('Instareferer', Instareferer::class);
        Blade::component('Login', Login::class);
        Blade::component('Navbar', Navbar::class);
        Blade::component('OrderDetails', OrderDetails::class);
        Blade::component('PersonalDetails', PersonalDetails::class);
        Blade::component('PetsCard', PetsCard::class);
        Blade::component('Productbox', Productbox::class);
        Blade::component('Productinfos', Productinfos::class);
        Blade::component('ProductPicView', ProductPicView::class);
        Blade::component('ProductsCommandDetails', ProductsCommandDetails::class);
        Blade::component('ProductSlidervertical', ProductSlidervertical::class);
        Blade::component('Service', Service::class);
        Blade::component('ShippingDetails', ShippingDetails::class);
        Blade::component('SignUp', SignUp::class);
        Blade::component('SimpleCard', SimpleCard::class);
        Blade::component('SingleProductPicView', SingleProductPicView::class);
        Blade::component('Slider', Slider::class);
        Blade::component('SmallCardSlide', SmallCardSlide::class);
        Blade::component('TabsNav', TabsNav::class);
        Blade::component('Topcard', Topcard::class);
        Blade::component('TopNavbar', TopNavbar::class);
        Blade::component('sectionText', sectionText::class);
    }
}
